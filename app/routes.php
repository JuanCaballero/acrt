<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});

Route::get('login', 'SessionsController@create');
Route::post('login', 'SessionsController@store');
Route::get('logout', 'SessionsController@destroy');

Route::get('/divisiones/{div}', 'DivisionsController@index');

Route::get('/equipos', 'TeamsController@index');
Route::get('/equipos/{teamId}', 'TeamsController@show')->where(array('teamId' =>'[0-9]{1,}'));

Route::get('/posiciones', 'PositionsController@index');
Route::get('/posiciones/{positionId}', 'PositionsController@show')->where(array('positionId' =>'[0-9]{1,}'));

Route::get('/tipos-habilidad', 'SkillTypesController@index');
Route::get('/tipos-habilidad/{skillTypeId}', 'SkillTypesController@show')->where(array('skillTypeId' =>'[0-9]{1,}'));

Route::get('/habilidades', 'SkillsController@index');
Route::get('/habilidades/{skillId}', 'SkillsController@show')->where(array('skillId' =>'[0-9]{1,}'));

Route::get('/razas', 'RacesController@index');
Route::get('/razas/{raceId}', 'RacesController@show')->where(array('raceId' =>'[0-9]{1,}'));

Route::get('/stats', 'StatsController@index');
Route::post('/stats', 'StatsController@update');

Route::Group(['before'=>'auth'], function(){
	Route::resource('/posiciones', 'PositionsController', array('except' => array('index', 'show')));
	Route::resource('/tipos-habilidad', 'SkillTypesController', array('except' => array('index', 'show')));
	Route::resource('/habilidades', 'SkillsController', array('except' => array('index', 'show')));
	Route::resource('/razas', 'RacesController', array('except' => array('index', 'show')));
	Route::resource('/equipos', 'TeamsController', array('except' => array('index', 'show')));
});

