<?php

class Skill extends \Eloquent {
	protected $fillable = ['*'];
	public $timestamps = false;

	public function type(){
		return $this->belongsTo('SkillType', 'skill_types_id');
	}

	public function positions(){
		return $this->belongsToMany('Position', 'positions_skills', 'skills_id', 'positions_id');
	}
}