<?php

class PositionsSkillTypesImprovementTypes extends \Eloquent {
	protected $fillable = [];
	public $timestamps = FALSE;
	public $table = 'positions_skill_types_improvement_types';

	public function position(){
		return $this->belongsTo('Position', 'positions_id');
	}

	public function skillType(){
		return $this->belongsTo('SkillType', 'skill_types_id');
	}

	public function improvementType(){
		return $this->belongsTo('ImprovementType', 'improvement_types_id');
	}
}