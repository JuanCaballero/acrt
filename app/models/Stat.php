<?php

class Stat extends \Eloquent {
	protected $fillable = [];
	public $table = 'stats';

	public function team(){
		return $this->belongsTo('Team', 'teams_id');
	}
}