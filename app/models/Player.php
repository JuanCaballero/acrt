<?php

class Player extends \Eloquent {
	protected $fillable = [];
	public $timestamps = FALSE;

	public function team(){
		return $this->belongsTo('Team');
	}

	public function position(){
		return $this->belongsTo('Position');
	}

	public function improvements(){
		return $this->hasMany('Improvement');
	}

	public function skills(){
		return $this->belongsToMany('Skill', 'players_skills', 'players_id', 'skills_id');
	}

	static function create(array $data = array()){
		unset($data['position']);

		$skills = array();
		foreach ($data['skills'] as $skill) {
			$skills[] = $skill->id;
		}
		unset($data['skills']);

		$player = parent::create($data);
		$player->skills()->sync($skills);
		return $player;
	}

	public function update(array $data = array()){
		unset($data['position']);

		$skills = array();
		foreach ($data['skills'] as $skill) {
			$skills[] = $skill->id;
		}
		unset($data['skills']);
		
		$this->skills()->sync($skills);
		return parent::update($data);
	}
}