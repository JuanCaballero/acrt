<?php

class Division extends \Eloquent {
	protected $fillable = [];
	public $timestamps = FALSE;

	public function teams(){
		return $this->hasMany('Team', 'divisions_id')
			->orderBy('points', 'DESC')
			->orderBy('td_diff', 'DESC')
			->orderBy('cas_diff', 'DESC');
	}
}