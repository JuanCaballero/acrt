<?php

class Position extends \Eloquent {
	protected $fillable = [];
	public $timestamps = FALSE;

	public function race(){
		return $this->belongsTo('Race', 'races_id');
	}

	public function skills(){
		return $this->belongsToMany('Skill', 'positions_skills', 'positions_id', 'skills_id');
	}

	public function positionsSkillTypesImprovementTypes(){
		return $this->hasMany('PositionsSkillTypesImprovementTypes', 'positions_id');
	}

	public function improvements(){
		return $this->positionsSkillTypesImprovementTypes()->with('skillType', 'improvementType');
	}

	public function simpleImprovements(){
		return $this->positionsSkillTypesImprovementTypes()->with('skillType')->where('improvement_types_id', 1);
	}
	public function doubleImprovements(){
		return $this->positionsSkillTypesImprovementTypes()->with('skillType')->where('improvement_types_id', 2);
	}

	public function validate($data){
		$rules = array(
			'title' => 'Required',
			'mo' => 'Required|Integer',
			'st' => 'Required|Integer',
			'ag' => 'Required|Integer',
			'ar' => 'Required|Integer',
			'cost' => 'Required|Integer',
			'max' => 'required|Integer',
		);

		return Validator::make($data, $rules);
	}

	public function updatePSTIT($data){
		$this->improvements()->delete();
		
		foreach (SkillType::where('key', "<>", "ex")->get() as $index => $skillType) {
			$PSTIT = new PositionsSkillTypesImprovementTypes;
			$PSTIT->positions_id = $this->id;
			$PSTIT->skill_types_id = $skillType->id;
			$PSTIT->improvement_types_id = $data['improvements'][$index];
			$PSTIT->save();
		}
	}

	public function delete(){
		$this->skills()->sync([]);
		$this->improvements()->delete();
		return parent::delete();
	}
}