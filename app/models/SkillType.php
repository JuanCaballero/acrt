<?php

class SkillType extends \Eloquent {
	protected $fillable = [];
	public $timestamps = FALSE;

	public function skills(){
		return $this->hasMany('Skill', 'skill_types_id');
	}
}