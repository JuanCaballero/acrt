<?php

class Race extends \Eloquent {
	protected $fillable = [];
	public $timestamps = FALSE;
	public $table = "races";

	public function positions(){
		return $this->hasMany('Position', 'races_id');
	}
}