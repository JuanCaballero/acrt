<?php

class Improvement extends \Eloquent {
	protected $fillable = [];
	public $timestamps = FALSE;

	public function player(){
		return $this->belongsTo('Player');
	}

	public function type(){
		return $this->belongsTo('ImprovementType');
	}

	public function Skill(){
		return $this->hasOne('Skill');
	}
}