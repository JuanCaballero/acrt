<?php

class Team extends \Eloquent {
	protected $fillable = ['*'];
	public $timestamps = FALSE;
	public $table = "teams";

	public function players(){
		return $this->hasMany('Player', 'teams_id');
	}

	public function race(){
		return $this->belongsTo('Race', 'races_id');
	}

	public function division(){
		return $this->belongsTo('Division', 'divisions_id');
	}

	public function validate($data){
		$rules = array(
			'title' => 'Required|unique:teams',
			'coach' => 'Required',
			'divisions_id' => 'Required',
			'races_id' => 'required'
		);

		return Validator::make($data, $rules);
	}

	public function update(array $data = array()){
		unset($data['_token']);
		unset($data['_method']);
		
		if (isset($data['players'])) {
			foreach ($data['players'] as $player) {
				$p = (array) json_decode($player);
				if($p['id'] === 'new'){
					$player = new Player;
					$player->unguard();
					unset($p['id']);
					$player->create($p);
				}else{
					$player = Player::find($p['id']);
					$player->unguard();
					$player->update($p);
				}
			}
		}
		unset($data['players']);
		$this->unguard();
		$data['td_diff'] = $data['bonus_td'] - $data['malus_td'];
		$data['cas_diff'] = $data['bonus_cas'] - $data['malus_cas'];
		return parent::update($data);
	}

	public function updatePlayers($data){
		return true;
	}
}