<?php

class StatsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /stats
	 *
	 * @return Response
	 */
	public function index()
	{
		$tds = Stat::where('stat_key', 'td')->with('team')->orderBy('quantity', 'DESC')->get();
		$coms = Stat::where('stat_key', 'com')->with('team')->orderBy('quantity', 'DESC')->get();
		$ints = Stat::where('stat_key', 'int')->with('team')->orderBy('quantity', 'DESC')->get();
		$cases = Stat::where('stat_key', 'cas')->with('team')->orderBy('quantity', 'DESC')->get();

		$teams = Team::lists('title', 'id');
		return View::make('statsForm', ['coms' =>$coms, 'tds' =>$tds, 'cases' =>$cases, 'ints' =>$ints, 'teams' => $teams]);
	}

	public function update($id)
	{
		//
	}
}