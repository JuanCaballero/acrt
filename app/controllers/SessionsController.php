<?php

class SessionsController extends \BaseController {
	/**
	 * Show the form for creating a new resource.
	 * GET /sessions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::check()){
			return View::make('home');
		}

		return View::make('Login');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /sessions
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::attempt(Input::only('username', 'password'))){
			return View::make('home');
		}

		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return View::make('home');
	}
}