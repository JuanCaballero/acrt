<?php

class SkillTypesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /skilltypes
	 *
	 * @return Response
	 */
	public function index()
	{
		$skillTypes = SkillType::orderBy('title')->get();
		return View::make('SkillTypesList', ['skillTypes' => $skillTypes]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /skilltypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /skilltypes
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /skilltypes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$skillType = SkillType::with('skills')->find($id);
		return View::make('SkillType', ['skillType' => $skillType]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /skilltypes/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /skilltypes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /skilltypes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}