<?php

class PositionsController extends \BaseController {
	private $model;
	public function __construct(Position $position){
		$this->model = $position;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /positions/create
	 *
	 * @return Response
	 */
	public function create()
	{		
		$race = Race::find(Input::get('races_id'));
		$skillTypes = SkillType::where('key', "<>", "ex")->get();
		$skills = Skill::orderBy('title')->lists('title', 'id');
		return View::make('PositionForm', ['race'=>$race, 'skillTypes' =>$skillTypes, 'skills' => $skills]);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /positions
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		$validation = $this->model->validate($data);
		
		if(!$validation->passes()){
			$race = Race::find(Input::get('races_id'));
			$skillTypes = SkillType::get();
			$skills = Skill::orderBy('title')->lists('title', 'id');
			return View::make('PositionForm', ['race'=>$race, 'skillTypes' =>$skillTypes, 'skills' => $skills, 'errors' => $validation->messages()->all()]);
		}

		$this->model = new Position;
		$this->model->races_id = $data['races_id'];
		$this->model->title = $data['title'];
		$this->model->slug = Str::slug($data['title']);
		$this->model->mo = $data['mo'];
		$this->model->st = $data['st'];
		$this->model->ag = $data['ag'];
		$this->model->ar = $data['ar'];
		$this->model->cost = $data['cost'];
		$this->model->max = $data['max'];
		$this->model->save();

		if(isset($data['skills'])) $this->model->skills()->sync($data['skills']);

		$this->model->updatePSTIT($data);

		return $this->edit($this->model->id);
	}
	/**
	 * Display the specified resource.
	 * GET /positions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /positions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$position = Position::with('race', 'skills')->with(['improvements'=>function($query){			
			$query->orderBy('skill_types_id');
		}])->find($id);
		$race = $position->race;
		$skills = Skill::orderBy('title')->lists('title', 'id');
		return View::make('PositionForm', ['race'=>$race, 'skills' => $skills, 'position' => $position]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /positions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();
		$position = $this->model->with('skills')->find($id);

		$position->title = $data['title'];
		$position->slug = Str::slug($data['title']);
		$position->mo = $data['mo'];
		$position->st = $data['st'];
		$position->ag = $data['ag'];
		$position->ar = $data['ar'];
		$position->cost = $data['cost'];
		$position->max = $data['max'];
		$position->save();

		$position->skills()->sync(isset($data['skills']) ? $data['skills'] : []);

		$position->updatePSTIT($data);

		return $this->edit($position->id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /positions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$position = $this->model->find($id);
		$race_id = $position->races_id;

		$position->delete();

		return Redirect::to("/razas/$race_id/edit");
	}

}