<?php

class TeamsController extends \BaseController {
	private $team;
	public function __construct(Team $team){
		$this->team = $team;
	}

	/**
	 * Display a listing of the resource.
	 * GET /teams
	 *
	 * @return Response
	 */
	public function index()
	{
		$teams = $this->team->orderBy('title')->get();
		return View::make('TeamsList', ['teams'=>$teams]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /teams/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$races = Race::orderBy('title')->get();
		$divisions = Division::orderBy('title')->get();
		return View::make('NewTeamForm', ['races' => $races, 'divisions' => $divisions]);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /teams
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();		
		$validation = $this->team->validate($data);
		if(!$validation->passes()){
			$races = Race::orderBy('title')->get();
			$divisions = Division::orderBy('title')->get();
			return View::make('NewTeamForm', ['races' => $races, 'divisions' => $divisions, 'errors' => $validation->messages()->all()]);
		}		
		unset($data['_token']);
		$this->team->unguard();
		$team = $this->team->create($data);
		return $this->edit($team->id);
	}

	/**
	 * Display the specified resource.
	 * GET /teams/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$team = $this->team->with('division', 'race')->with(['players'=>function($query){
			$query->orderBy('number');
		}])->find($id);
		$divisions = Division::lists('title', 'id');
		$races = Race::orderBy('title')->lists('title', 'id');
		return View::make('Team', ['team'=>$team, 'divisions'=>$divisions, 'races'=>$races]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /teams/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$divisions = Division::orderBy('title')->lists('title', 'id');
		//$skills = Skill::orderBy('title')->select('title', 'skill_types_id', 'id')->get();
		$skillTypes = SkillType::with(['skills'=>function($query){
			$query->orderBy('title');
			$query->select('title', 'skill_types_id', 'id');
		}])->get();
		
		$team = $this->team->with('race', 'race.positions', 'race.positions.skills', 'players', 'players.skills')->find($id);
		$positionsList = $team->race->positions->lists('title', 'id');
		return View::make('TeamForm', ['divisions' => $divisions, 'team' => $team, 'positionsList' => $positionsList, 'skillTypes'=>$skillTypes]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /teams/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();		
		$team = $this->team->find($id);
		$team->update($data);
		return $this->edit($team->id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /teams/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}