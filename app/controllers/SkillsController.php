<?php

class SkillsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /skills
	 *
	 * @return Response
	 */
	public function index()
	{
		$skills = Skill::with('type')->get();

		return View::make('SkillsList', ['skills' => $skills]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /skills/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$skillTypes = SkillType::orderBy('title')->lists('title', 'id');
		return View::make('SkillForm', ['skillTypes' => $skillTypes]);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /skills
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		
		$skill = new Skill;
		$skill->title = $data['title'];
		$skill->slug = Str::slug($data['title']);
		$skill->skill_types_id = $data['skill_types_id'];
		$skill->text = $data['text'];
		$skill->save();
		
		return $this->edit($skill->id);
	}

	/**
	 * Display the specified resource.
	 * GET /skills/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$skill = Skill::with('type')->find($id);
		return View::make('Skill', ['skill'=>$skill]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /skills/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$skill = Skill::with('type')->find($id);
		$skillTypes = SkillType::orderBy('title')->lists('title', 'id');
		return View::make('SkillForm', [ 'skill'=>$skill , 'skillTypes' => $skillTypes]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /skills/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$skill = Skill::find($id);		
		$skill->title = $data['title'];
		$skill->slug = Str::slug($data['title']);
		$skill->skill_types_id = $data['skill_types_id'];
		$skill->text = $data['text'];
		$skill->save();

		return $this->edit($skill->id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /skills/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$skill = Skill::find($id);
		$oldSkill = $skill->toArray();
		$skill->delete();
		return View::make('DeletedSkill', ['oldSkill' => $oldSkill]);
	}
}