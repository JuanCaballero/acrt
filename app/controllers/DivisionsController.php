<?php

class DivisionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /divisions
	 *
	 * @return Response
	 */
	public function index($division_id)
	{
		$division = Division::with(['teams'])->find($division_id);
		return View::make('Division', ['division'=>$division]);
	}
}