<?php

class RacesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /races
	 *
	 * @return Response
	 */
	public function index()
	{
		$races = Race::orderBy('title')->lists('title', 'id');

		return View::make('RacesList', ['races'=>$races]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /races/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('RaceForm');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /races
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		$race = new Race;
		$race->title = $data['title'];
		$race->allow_apo = $data['allow_apo'];
		$race->rr_cost = $data['rr_cost'];
		$race->save();

		return $this->edit($race->id);
	}

	/**
	 * Display the specified resource.
	 * GET /races/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$race = Race::with(['positions', 'positions.skills', 'positions.simpleImprovements', 'positions.doubleImprovements'])->find($id);
		return View::make('Race', ['race'=>$race]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /races/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$race = Race::with(['positions', 'positions.skills', 'positions.simpleImprovements', 'positions.doubleImprovements'])->find($id);
		return View::make('RaceForm', ['race'=>$race]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /races/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();
		$race = Race::find($id);
		unset($data['_method']);
		unset($data['_token']);
		$race->unguard();
		$race->update($data);

		return $this->edit($race->id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /races/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}