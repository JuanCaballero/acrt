<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprovements extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('improvement_types', function(Blueprint $table){
			$table->increments('id');
			$table->string('title', 10);
			$table->integer('amount');
		});

		Schema::create('improvements', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('players_id');
			$table->integer('improvement_types_id');
			$table->integer('skills_id');
			$table->integer('added_cost');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('improvement_types');
		Schema::drop('improvements');		
	}

}
