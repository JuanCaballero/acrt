<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkills extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 50);
			$table->string('slug', 50);
			$table->text('text');
			$table->integer('skill_types_id');
		});

		Schema::create('skill_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 20);
			$table->string('slug', 20);
			$table->string('key', 2);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skills');
		Schema::drop('skill_types');
	}

}
