<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeams extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teams', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('coach');
			$table->integer('races_id');
			$table->integer('ff');
			$table->integer('rrs');
			$table->integer('assistants');
			$table->integer('cheerleaders');
			$table->boolean('has_apo');
			$table->integer('divisions_id');
			$table->integer('treasure');
			$table->integer('team_value');
			$table->integer('games');
			$table->integer('wins');
			$table->integer('losses');
			$table->integer('draws');
			$table->integer('bonus_td');
			$table->integer('malus_td');
			$table->integer('td_diff');
			$table->integer('bonus_cas');
			$table->integer('malus_cas');
			$table->integer('cas_diff');
			$table->integer('points');
			$table->text('comments');
		});

		Schema::create('divisions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teams');
		Schema::drop('divisions');
	}

}
