<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('players', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('number');
			$table->string('title', 60);
			$table->integer('positions_id');
			$table->integer('teams_id');
			$table->integer('races_id');
			$table->integer('mo');
			$table->integer('st');
			$table->integer('ag');
			$table->integer('ar');
			$table->string('injuries');
			$table->integer('com');
			$table->integer('td');
			$table->integer('cas');
			$table->integer('int');
			$table->integer('mvp');
			$table->integer('total_px');
			$table->integer('cost');
			$table->string('state');
		});

		Schema::create('players_skills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('players_id');
			$table->integer('skills_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('players');
		Schema::drop('players_skills');
	}

}
