<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRaces extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('races', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 50);
			$table->string('slug', 50);
			$table->boolean('allow_apo');
			$table->integer('rr_cost');
		});

		Schema::create('positions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 50);
			$table->string('slug', 50);
			$table->integer('mo');
			$table->integer('st');
			$table->integer('ag');
			$table->integer('ar');
			$table->integer('cost');
			$table->integer('races_id');
			$table->integer('max');
		});

		Schema::create('positions_skills', function(Blueprint $table){
			$table->increments('id');
			$table->integer('skills_id');
			$table->integer('positions_id');
		});

		Schema::create('positions_skill_types_improvement_types', function(Blueprint $table){
			$table->increments('id');
			$table->integer('positions_id');
			$table->integer('skill_types_id');
			$table->integer('improvement_types_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('races');
		Schema::drop('positions');
		Schema::drop('positions_skills');
		Schema::drop('positions_skill_types_improvement_types');
	}

}
