<?php

class DivisionsTableSeeder extends Seeder
{
	
	public function run()
	{
		DB::table('divisions')->delete();
		DB::table('divisions')->insert(array(
			array('id' => 1,'title' => 'Primera Division'),
			array('id' => 2,'title' => 'Segunda Division')
		));
	}
}


	
