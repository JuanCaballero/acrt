<?php

class SkillTypesTableSeeder extends Seeder
{
	
	public function run()
	{
		DB::table('skill_types')->delete();
		DB::table('skill_types')->insert(array(
			array('id' => 1, 'title' => 'General', 'slug' => 'general', 'key' => 'ge'),
			array('id' => 2, 'title' => 'Fuerza', 'slug' => 'fuerza', 'key' => 'st'),
			array('id' => 3, 'title' => 'Agilidad', 'slug' => 'agilidad', 'key' => 'ag'),
			array('id' => 4, 'title' => 'Pase', 'slug' => 'pase', 'key' => 'ps'),
			array('id' => 5, 'title' => 'Mutación', 'slug' => 'mutacion', 'key' => 'mu'),
			array('id' => 6, 'title' => 'Extraordinaria', 'slug' => 'extraordinaria', 'key' => 'ex')

		));
	}
}


	
