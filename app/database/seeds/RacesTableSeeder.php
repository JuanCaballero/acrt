<?php

class RacesTableSeeder extends Seeder
{
	
	public function run()
	{
		DB::table('races')->delete();
		DB::table('races')->insert(array(			
			array('id' => 1, 'title' => 'Humanos', 'slug' => 'humanos', 'allow_apo' => 1, 'rr_cost' => '50000'),
			array('id' => 2, 'title' => 'Skaven', 'slug' => 'skaven', 'allow_apo' => 1, 'rr_cost' => '60000'),
			array('id' => 3, 'title' => 'Orcos', 'slug' => 'orcos', 'allow_apo' => 1, 'rr_cost' => '600000'),
			array('id' => 4, 'title' => 'Enanos', 'slug' => 'enanos', 'allow_apo' => 1, 'rr_cost' => '50000'),
			array('id' => 5, 'title' => 'Enanos del caos', 'slug' => 'enanos-del-caos', 'allow_apo' => 1, 'rr_cost' => '60000'),
			array('id' => 6, 'title' => 'Altos Elfos', 'slug' => 'altos-elfos', 'allow_apo' => 1, 'rr_cost' => '50000'),
			array('id' => 7, 'title' => 'Nigromantes', 'slug' => 'nigromantes', 'allow_apo' => 0, 'rr_cost' => '70000'),
			array('id' => 8, 'title' => 'No muertos', 'slug' => 'no-muertos', 'allow_apo' => 0, 'rr_cost' => '70000'),
			array('id' => 9, 'title' => 'Nórdicos', 'slug' => 'nordicos', 'allow_apo' => 1, 'rr_cost' => '60000'),
			array('id' => 10, 'title' => 'Khemri', 'slug' => 'khemri', 'allow_apo' => 0, 'rr_cost' => '70000'),
			array('id' => 11, 'title' => 'Hombres lagarto', 'slug' => 'hombres-lagarto', 'allow_apo' => 1, 'rr_cost' => '60000'),
			array('id' => 12, 'title' => 'Vampiros', 'slug' => 'vampiros', 'allow_apo' => 1, 'rr_cost' => '70000'),
			array('id' => 13, 'title' => 'Ogros', 'slug' => 'ogros', 'allow_apo' => 1, 'rr_cost' => '70000'),
			array('id' => 14, 'title' => 'Nurgle', 'slug' => 'nurgle', 'allow_apo' => 0, 'rr_cost' => '70000'),
			array('id' => 15, 'title' => 'Halflings', 'slug' => 'halflings', 'allow_apo' => 0, 'rr_cost' => '60000'),
			array('id' => 16, 'title' => 'Goblins', 'slug' => 'goblins', 'allow_apo' => 1, 'rr_cost' => '60000'),
			array('id' => 17, 'title' => 'Caos', 'slug' => 'caos', 'allow_apo' => 1, 'rr_cost' => '60000'),
			array('id' => 18, 'title' => 'Elfos profesionales', 'slug' => 'elfos-profesionales', 'allow_apo' => 1, 'rr_cost' => '50000'),
			array('id' => 19, 'title' => 'Elfos oscuros', 'slug' => 'elfos-oscuros', 'allow_apo' => 1, 'rr_cost' => '50000'),
			array('id' => 20, 'title' => 'Elfos silvanos', 'slug' => 'elfos-silvanos', 'allow_apo' => 1, 'rr_cost' => '50000'),
			array('id' => 21, 'title' => 'Amazonas', 'slug' => 'amazonas', 'allow_apo' => 1, 'rr_cost' => '50000')
		));
	}
}


	
