<?php

class ImprovementTypesTableSeeder extends Seeder
{
	
	public function run()
	{
		DB::table('improvement_types')->delete();
		DB::table('improvement_types')->insert(array(
			array('id' => 1,'title' => 'Simples', 'amount' => 20000),
			array('id' => 2,'title' => 'Dobles', 'amount' => 30000),
			array('id' => 3,'title' => '+Mo / +Ar', 'amount' => 30000),
			array('id' => 4,'title' => '+Ag', 'amount' => 40000),
			array('id' => 5,'title' => '+Fu', 'amount' => 50000)
		));
	}
}


	
