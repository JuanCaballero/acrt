<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('SkillTypesTableSeeder');
		$this->call('SkillsTableSeeder');
		$this->call('RacesTableSeeder');
		$this->call('PositionsTableSeeder');
		$this->call('PositionsSkillsTableSeeder');
		$this->call('ImprovementTypesTableSeeder');
		$this->call('PositionSkillTypesImprovementTypesTableSeeder');
		$this->call('DivisionsTableSeeder');
		$this->call('UsersTableSeeder');
	}

}
