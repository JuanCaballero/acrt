@extends ('layouts.main')

@section('content')
	<div class="container content">
		@if (isset($skill))
			<h1>{{$skill->title}} <small>({{$skill->type->title}})</small></h1>
			{{Form::model($skill, ['method'=>'PUT', 'route'=>['habilidades.update', $skill->id] ])}}
		@else
			<h1>Nueva habilidad</h1>
			{{Form::open(['url'=>'/habilidades', 'method'=>'POST'])}}
		@endif		
			<div class="form-group">
				{{Form::text('title', null, ['class'=>'form-control'])}}	
			</div>
			<div class="form-group">
				{{Form::select('skill_types_id', $skillTypes, null, ['class'=>'form-control'])}}	
			</div>
			<div class="form-group">
				{{Form::textarea('text', null, ['class'=>'form-control'])}}
			</div>
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="/habilidades" class="btn btn-primary">Ir a la lista</a>
		{{Form::close()}}
	</div>
@stop