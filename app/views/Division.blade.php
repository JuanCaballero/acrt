@extends ('layouts.main')

@section('content')
	<div class="container">
		<h1>{{$division->title}}</h1>
		<h2><span class="label label-danger">Clasificación</span></h2>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Posicion</th>
						<th>Jornada</th>
						<th>Vic</th>
						<th>Emp</th>
						<th>Der</th>
						<th>Nombre</th>
						<th>Entrenador</th>
						<th>+Td</th>
						<th>-Td</th>
						<th>+Her</th>
						<th>-Her</th>
						<th>Pts</th>
					</tr>
				</thead>
				<tbody>
					@foreach($division->teams as $num => $team)
						<tr>
							<td>{{$num + 1}}</td>
							<td>{{$team->games}}</td>
							<td>{{$team->wins}}</td>
							<td>{{$team->draws}}</td>
							<td>{{$team->losses}}</td>
							<td>{{$team->title}}</td>
							<td>{{$team->coach}}</td>
							<td>{{$team->bonus_td}}</td>
							<td>{{$team->malus_td}}</td>
							<td>{{$team->bonus_cas}}</td>
							<td>{{$team->malus_cas}}</td>
							<td>{{$team->points}}</td>
						</tr>
					@endforeach					
				</tbody>
			</table>
		</div>
		<h2><span class="label label-danger">Estadísticas</span></h2>
		<div class="gap"></div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Touchdowns</h4>
			</div>
			<div class="panel-body">
				<div class="table">
					<table class="table">
						<thead>
							<tr>
								<th>Posición</th>
								<th>Jugador</th>
								<th>Equipo</th>
								<th>Cantidad</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>			
		</div>
		<div class="gap"></div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Heridas</h4>
			</div>
			<div class="panel-body">
				<div class="table">
					<table class="table">
						<thead>
							<tr>
								<th>Posición</th>
								<th>Jugador</th>
								<th>Equipo</th>
								<th>Cantidad</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>			
		</div>
		<div class="gap"></div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Pases completos</h4>
			</div>
			<div class="panel-body">
				<div class="table">
					<table class="table">
						<thead>
							<tr>
								<th>Posición</th>
								<th>Jugador</th>
								<th>Equipo</th>
								<th>Cantidad</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>			
		</div>	
		<div class="gap"></div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Intercepciones</h4>
			</div>
			<div class="panel-body">
				<div class="table">
					<table class="table">
						<thead>
							<tr>
								<th>Posición</th>
								<th>Jugador</th>
								<th>Equipo</th>
								<th>Cantidad</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>			
		</div>
	</div>
@stop