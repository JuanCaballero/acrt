<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Juan A. Caballero">
		@section('title')
			<title>Liga ACRT de Blood Bowl - Temporada 18</title>
		@show
		@section('meta-description')
	    	<meta name="description" content="Página de la liga de Blood Bowl ACRT, Liga de Blood Bowl en Alicante. Somos una de las ligas de juego presencial más grandes de españa, con entre 20 y 28 participantes cada temporada">
		@show

		@section('meta-keywords')
			<meta name="keywords" content="Blood Bowl, Liga Blood Bowl, Blood Bowl Alicante, BloodBowl, BloodBowl Alicante, Liga BloodBowl">
		@show

		<link rel="stylesheet" href="/styles/bootstrap.min.css">
		<link rel="stylesheet" href="/styles/bootstrap-theme.min.css">
  		<link rel="stylesheet" href="/styles/custom-theme/jquery-ui-1.10.3.custom.css">
  		<link rel="stylesheet" href="/styles/main.css">

  		@yield('styles')
	</head>
	<body role="document" data-feedly-mini="yes">
		<header>
			<div class="navbar navbar-inverse" role="navigation" id="main-menu">
		      	<div class="container">
		        	<div class="navbar-header">
		          		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
		          		</button>
		          		<a class="navbar-brand" href="/">Liga ACRT</a>
		        	</div>
		        	@if (Auth::check())
		        		<div class="navbar-brand navbar-right">{{Auth::user()->username}} - <a href="/logout">Desconectar</a></div>
		        	@else
		        		{{ Form::open(['url'=>'login', 'method'=>'POST', 'class'=>'navbar-form navbar-right form-login', 'role'=>'form']) }}
			        	    <div class="form-group">
			        	    	{{Form::text('username', null, ['class'=>'form-control input-sm', 'placeholder'=>'Usuario'])}}
				            </div>
				            <div class="form-group">
				              {{Form::password('password', ['class'=>'form-control input-sm', 'placeholder'=>'Contraseña'])}}
				            </div>
				            <button type="submit" class="btn btn-success btn-sm">Sign in</button>
				        {{Form::close()}}
		        	@endif
		        	
			        <div class="navbar-collapse collapse">
		          		<ul class="nav navbar-nav">
		          			<li><a href="#contact">Calendario</a></li>
				            <li class="dropdown">
				            	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Clasi - Stats <span class="caret"></span></a>
				            	<ul class="dropdown-menu" role="menu">
				            		<li><a href="/divisiones/1">Primera División</a></li>
				            		<li><a href="/divisiones/2">Segunda División</a></li>
				            	</ul>
				            </li>
		          		</ul>
		        	</div><!--/.nav-collapse -->
		      	</div>
		    </div>
		    @if (Auth::check())
		    	<div class="container">
			    	<ul class="list-unstyled list-inline">
			    		<li><a href="/equipos">Equipos</a></li>
			    		<li><a href="/stats">Estadísticas</a></li>
			    		<li><a href="/razas">Razas</a></li>
			    		<li><a href="/tipos-habilidad">Tipos de habilidad</a></li>
			    		<li><a href="/habilidades">Habilidades</a></li>
			    	</ul>
		    	</div>
		    @endif
		</header>
		@yield('content')			
		<footer>
			
		</footer>
		<script src="/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
		<script src="/scripts/bootstrap.js" type="text/javascript"></script>
		<script src="/scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

		@yield('scripts')
	</body>
</html>