@extends ('layouts.main')

@section('content')
	<div class="container">
		<h1>Habilidades de {{$skillType->title}}</h1>
		<ul>
			@foreach($skillType->skills as $skill)
				<li><a href="/habilidades/{{$skill->id}}">{{$skill->title}} ({{$skill->type->title}})</a></li>
			@endforeach
		</ul>
	</div>
@stop