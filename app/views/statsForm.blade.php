@extends ('layouts.main')

@section('content')
	<div class="container">
		<h1><span class="label label-danger">Estadísticas</span></h1>
		<div class="gap"></div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Touchdowns</h4>
			</div>
			<div class="panel-body">
				<div class="table">
					<table class="table" id="tds">
						<thead>
							<tr>
								<th>Posición</th>
								<th>Jugador</th>
								<th>Equipo</th>
								<th>Cantidad</th>
								<th><span id="addStat" class="btn btn-success btn-sm">+</span></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($tds as $key => $td): ?>
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$td->player}}</td>
									<td>{{$td->team->title}}</td>
									<td>{{$td->quantity}}</td>
									<td><span class="btn btn-danger btn-sm">x</span></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>			
		</div>
		
	</div>
@stop