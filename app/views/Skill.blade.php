@extends ('layouts.main')

@section('content')
	<div class="container content">
		<h1>{{$skill->title}} <small>({{$skill->type->title}})</small></h1>
		<p>{{$skill->text}}</p>
		{{Form::model($skill, ['method'=>'GET', 'route'=>['habilidades.edit', $skill->id] ])}}
			<button type="submit" class="btn btn-primary">Editar</button>
			<a href="/habilidades" class="btn btn-primary">Ir a la lista</a>
		{{Form::close()}}

	</div>
@stop