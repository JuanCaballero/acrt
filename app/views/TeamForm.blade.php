@extends ('layouts.main')

@section('styles')
	<link rel="stylesheet" href="/styles/form.css">
@stop
@section('content')
	<div class="container" id="formContainer">
		<h1 class="align-center">{{$team->title}}</h1>
		<div class="form" role="form" id="teamForm">
			{{Form::model($team, ['method'=>'PATCH', 'route' => ['equipos.update', $team->id] ])}}
				{{Form::token()}}
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="title">Nombre</label>
							{{Form::text('title', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="coach">Entrenador</label>
							{{Form::text('coach', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="divisions_id">División</label>
							{{Form::select('divisions_id', $divisions, $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="races_id">Raza</label>
							{{Form::text('races_id', $team->race->title, $attributes = array('class' => 'form-control', 'disabled'=>'disabled'))}}
						</div>
						<div class="form-group">
							<label for="trasure">Tesorería</label>
							{{Form::text('treasure', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
					</div>
					<div class="col-md-3">						
						<div class="form-group">
							<label for="ff">Factor de hinchas</label>
							{{Form::text('ff', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="rrs">Rerrolls ({{number_format($team->race->rr_cost, 0, ',', '.')}} mo.)</label>
							{{Form::text('rrs', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						
						<div class="form-group">
							<label for="assistants">Ayudantes</label>
							{{Form::text('assistants', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="cheerleaders">Animadoras</label>
							{{Form::text('cheerleaders', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="has_apo">Apotecario</label>
							{{Form::select('has_apo', array('1'=>'Sí', 0 =>'No'), $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
					</div>
					<div class="col-md-3">
						
						<div class="form-group">
							<label for="team_value">Valor de equipo</label>
							{{Form::text('team_value', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="games">Jornada de liga</label>
							{{Form::text('games', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="wins">Partidos ganados (liga)</label>
							{{Form::text('wins', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="draws">Partidos empatados (liga)</label>
							{{Form::text('draws', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="losses">Partidos perdidos (liga)</label>
							{{Form::text('losses', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="bonus_td">Tds a favor (liga)</label>
							{{Form::text('bonus_td', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="malus_td">Tds en contra (liga)</label>
							{{Form::text('malus_td', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="bonus_cas">Heridas a favor (liga)</label>
							{{Form::text('bonus_cas', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="malus_cas">Heridas en contra (liga)</label>
							{{Form::text('malus_cas', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
						<div class="form-group">
							<label for="points">Puntos de liga</label>
							{{Form::text('points', $value = null, $attributes = array('class' => 'form-control'))}}
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="comments">Comentarios</label>
					{{Form::textarea('comments', $value = null, $attributes = array('class' => 'form-control'))}}
				</div>
				<h3>Jugadores</h3>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Num.</th>
								<th>Nombre</th>
								<th>Posición</th>
								<th>Mo</th>
								<th>Fu</th>
								<th>Ag</th>
								<th>Ar</th>
								<th>Habilidades</th>
								<th>Les</th>
								<th>Com</th>
								<th>Td</th>
								<th>Her</th>
								<th>Int</th>
								<th>MVP</th>
								<th>Px</th>
								<th>Val</th>
							</tr>
						</thead>
						<tbody>
							@for ($i=1; $i < 17; $i++) 
								<tr id="{{$i}}">
									<td>{{$i}} <span id="playersModal" class="btn btn-success btn-sm">+</span></td>
									<td></td>
									<td></td>								
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							@endfor
							
						</tbody>
					</table>
				</div>
				
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Enviar">					
				</div>
			{{Form::close()}}
		</div>
		<div id="addPlayerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4>Formulario Jugador</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							{{Form::text('playerName', null, array('placeholder'=>'Nombre', 'class'=>'form-control'))}}
						</div>
						<div class="form-group">
							{{Form::select('positions_id', $positionsList, null, array('class'=>'form-control'))}}
						</div>
						
					</div>
					<div class="modal-footer">
						<button class="btn btn-success" type="button" id="addPlayer" data-number="">Guardar</button>
						<button class="btn btn-danger" data-dismiss="modal" type="button">Cancelar</button>
					</div>
				</div>
			</div>
		</div>
		<div id="editFieldModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
		<div id="editSkillsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
	</div>	
@stop

@section('scripts')
	<script src="/scripts/monocle.1.0.2/monocle.js"></script>
	<script src="/scripts/app/controllers/teamFormController.js"></script>
	<script src="/scripts/app/models/position.js"></script>
	<script>
		__Controller.teamFormController = new TeamFormController("#formContainer", <?php  echo $team->toJson() ?>, <?php  echo $skillTypes->toJson() ?>);
	</script>
@stop