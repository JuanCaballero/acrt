@extends ('layouts.main')

@section('content')
	<div class="container">
		<h1>Razas <a href="/razas/create" class="btn btn-success">Añadir nueva</a></h1>
		
		<ul>
			@foreach($races as $id => $race)
				<li><a href="/razas/{{$id}}">{{$race}}</a></li>
			@endforeach
		</ul>
	</div>
@stop