@extends ('layouts.main')

@section('styles')
	<link rel="stylesheet" href="/styles/teamForm.css">
@stop
@section('content')
	<div class="container">
		<h1 class="align-center">{{$team->title}}</h1>
		<div>
			<ul>
				<li>Entrenador: {{$team->coach}}</li>
				<li>División: {{$team->division->title}}</li>
				<li>Raza: {{$team->race->title}}</li>
				<li>Tesorería: {{$team->treasure}} mo.</li>
				<li>Rerrolls: {{$team->rrs}}</li>
				<li>Factor de hinchas: {{$team->ff}}</li>
				<li>Ayudantes del entrenador: {{$team->assistants}}</li>
				<li>Animadoras: {{$team->cheerleaders}}</li>
				<li>Médico: {{$team->has_apo}}</li>
				<li>Valor de equipo: {{$team->team_value}}</li>
				<li>Jornada de liga: {{$team->games}}</li>
				<li>Partidos ganados: {{$team->wins}}</li>
				<li>Partidos perdidos: {{$team->losses}}</li>
				<li>Partidos empatados: {{$team->draws}}</li>
				<li>Touchdowns a favor (liga): {{$team->bonus_td}}</li>
				<li>Touchdowns en contra (liga): {{$team->malus_td}}</li>
				<li>Heridas a favor (liga): {{$team->bonus_cas}}</li>
				<li>Heridas en contra (liga): {{$team->malus_cas}}</li>
				<li>Puntos en liga: {{$team->points}}</li>
				<li>Comentarios: {{$team->comments}}</li>
			</ul>
		</div>
		<h3>Jugadores</h3>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Num.</th>
						<th>Nombre</th>
						<th>Posición</th>
						<th>Mo</th>
						<th>Fu</th>
						<th>Ag</th>
						<th>Ar</th>
						<th>Habilidades</th>
						<th>Les</th>
						<th>Com</th>
						<th>Td</th>
						<th>Her</th>
						<th>Int</th>
						<th>MVP</th>
						<th>Px</th>
						<th>Val</th>
					</tr>
				</thead>
				<tbody>
					@for ($i=1; $i < 17; $i++) 
						<tr id="number-{{$i}}">
							<td>{{$i}}<label id="add" class="text-success">+</label></td>
							<td id="{{$i}}-name"></td>
							<td id="{{$i}}-position"></td>								
							<td id="{{$i}}-mo"></td>
							<td id="{{$i}}-fu"></td>
							<td id="{{$i}}-ag"></td>
							<td id="{{$i}}-ar"></td>
							<td id="{{$i}}-skills"></td>
							<td id="{{$i}}-inj"></td>
							<td id="{{$i}}-com"></td>
							<td id="{{$i}}-td"></td>
							<td id="{{$i}}-cas"></td>
							<td id="{{$i}}-int"></td>
							<td id="{{$i}}-mvp"></td>
							<td id="{{$i}}-total_px"></td>
							<td id="{{$i}}-cost"></td>
						</tr>
					@endfor
					
				</tbody>
			</table>
		</div>
		{{Form::model($team, ['method'=>'GET', 'route' => ['equipos.edit', $team->id] ])}}
			{{Form::submit('Editar', ['class'=>'btn btn-primary'])}}
			<a href="/equipos" class="btn btn-primary">Volver</a>
		{{Form::close()}}
	</div>
@stop

@section('scripts')
	<script src="/scripts/monocle.1.0.2/monocle.js"></script>
	<script src="/scripts/app/controllers/teamFormController.js"></script>
	<script src="/scripts/app/models/team.js"></script>
	<script src="/scripts/app/models/race.js"></script>
	<script src="/scripts/app/models/position.js"></script>
	<script>
		console.log( <?php  echo $team->toJson() ?>);
		__Controller.teamFormController = new TeamFormController("#teamForm", <?php  echo $team->toJson() ?>);
	</script>
@stop