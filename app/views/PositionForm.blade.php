@extends ('layouts.main')

@section('styles')
	<link rel="stylesheet" href="/styles/form.css">
@stop

@section('content')
	<div class="container content">
		<div class="form" role="form form-inline">
			@if (isset($position))
				<h1>{{$position->title}} <small>({{$race->title}})</small></h1>
				{{Form::model($position, ['method'=>'PUT', 'route'=>['posiciones.update', $position->id], 'class'=>"form-inline" ])}}
			@else
				<h1>Nueva posición para {{$race->title}}</h1>
				{{Form::open(['url'=>'/posiciones', 'method'=>'POST', 'class'=>"form-inline"])}}
			@endif
				@foreach ($errors as $error)
					<div class="text-danger">{{$error}}</div>
				@endforeach
				{{Form::hidden('races_id', $race->id)}}			
				<div class="form-group">
				{{Form::label('title', 'Posición', ['class' => 'form-label'])}}
				{{Form::text('title', null, ['class'=>'form-control input-sm'])}}	
				</div>
				<div class="form-group numberSmall">
					{{Form::label('mo', 'Mo', ['class' => 'form-label'])}}
					{{Form::text('mo', null, ['class'=>'form-control input-sm'])}}	
				</div>
				<div class="form-group numberSmall">
					{{Form::label('st', 'Fu', ['class' => 'form-label'])}}
					{{Form::text('st', null, ['class'=>'form-control input-sm'])}}
				</div>
				<div class="form-group numberSmall">
					{{Form::label('ag', 'Ag', ['class' => 'form-label'])}}
					{{Form::text('ag', null, ['class'=>'form-control input-sm'])}}	
				</div>
				<div class="form-group numberSmall">
					{{Form::label('ar', 'Ar', ['class' => 'form-label'])}}
					{{Form::text('ar', null, ['class'=>'form-control input-sm'])}}	
				</div>
				<div class="form-group numberSmall">
					{{Form::label('cost', 'Coste', ['class' => 'form-label'])}}
					{{Form::text('cost', null, ['class'=>'form-control input-sm'])}}	
				</div>
				<div class="form-group numberSmall">
					{{Form::label('max', 'Máximo', ['class' => 'form-label'])}}
					{{Form::text('max', null, ['class'=>'form-control input-sm'])}}
				</div>
				<h3>Habilidades</h3>
				<div id="skillsContainer">
					@if (isset($position))
						@foreach ($position->skills as $skill)
							{{Form::hidden('skills[]', $skill->id, array('id'=>'skills'))}}
						@endforeach
					@endif	
					<ul id="skillsList" class="list-unstyled">
						@if (isset($position))
							@foreach ($position->skills as $skill)
								<li id="{{$skill->id}}">
									<h3>
										<span class='label label-primary'>{{$skill->title}}</span>
										<span id='removeSkill' data-id='{{$skill->id}}' class='label label-danger'>x</span>
									</h3>
								</li>
							@endforeach
						@endif							
					</ul>					
					<div class="form-group">
						{{Form::select('', $skills, null, ['class'=>'form-control inline input-sm', 'id'=>'skillsSelector'])}}	
					</div>						
					<a id="addSkill" class="btn btn-success btn-sm">+</a>
				</div>
				<h3>Subidas</h3>
				<div>
					@if (isset($position))
						@foreach ($position->improvements as $improvement)
							<div class="form-group select-small">
								<label for="improvements[]">{{$improvement->skillType->title}}</label>
								
								{{Form::select('improvements[]', [6=>'No disponible', 1=>'simples', 2=>'dobles'], $improvement->improvementType['id'], ['class'=>'form-control input-sm select'])}}
							</div>
						@endforeach
					@else
						@foreach ($skillTypes as $skillType)
							<div class="form-group select-small">
								<label for="improvements[]">{{$skillType->title}}</label>
								{{Form::select('improvements[]', [6=>'No disponible', 1=>'simples', 2=>'dobles'], null, ['class'=>'form-control input-sm'])}}
							</div>
						@endforeach
					@endif
				</div>
				<h3></h3>
				<button type="submit" class="btn btn-success">Guardar</button>
				<a href="/razas/{{$race->id}}/edit" class="btn btn-primary">Volver</a>
			{{Form::close()}}
		</div>	
	</div>
@stop

@section('scripts')
	<script src="/scripts/monocle.1.0.2/monocle.js"></script>
	<script src="/scripts/app/controllers/positionFormController.js"></script>
@stop