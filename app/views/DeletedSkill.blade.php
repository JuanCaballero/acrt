@extends ('layouts.main')

@section('content')
	<div class="container content">
		<h1 class="text-danger">La habilidad {{$oldSkill['title']}} ha sido eliminada</h1>
		<a href="/habilidades">Volver a la lista</a>
	</div>
@stop