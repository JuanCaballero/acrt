@extends ('layouts.main')

@section('content')
	<div class="container content">
		<h1>{{$race->title}}</h1>
		<p><strong>Puede fichar Apotecario</strong>: {{$race->allow_apo ? "Sí" : "No"}}</p>
		<p><strong>Valor de los Reroll</strong>: {{$race->rr_cost}}</p>
		<h3>Posiciones:</h3>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Mo</th>
						<th>Fu</th>
						<th>Ag</th>
						<th>Ar</th>
						<th>Habilidades</th>
						<th>Coste</th>
						<th>Simp/Dob</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($race->positions as $position)
						<tr>
							<td>{{$position->title}}</td>
							<td>{{$position->mo}}</td>
							<td>{{$position->st}}</td>
							<td>{{$position->ag}}</td>
							<td>{{$position->ar}}</td>
							<td>
								@foreach ($position->skills as $skill)
									·<a href="/habilidades/{{$skill->id}}">{{$skill->title}}</a>
								@endforeach
							</td>
							<td>{{number_format($position->cost, 0, "", ".")}} <strong>mo.</strong></td>
							<td>
								@foreach ($position->simpleImprovements as $simple)
									·<a href="/tipos-habilidad/{{$simple->skill_type->id}}">{{$simple->skill_type->key}}</a>
								@endforeach
								/
								@foreach ($position->doubleImprovements as $double)
									·<a href="/tipos-habilidad/{{$double->skill_type->id}}">{{$double->skill_type->key}}·</a>
								@endforeach
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		
		{{Form::model($race, ['method'=>'GET', 'route'=>['razas.edit', $race->id] ])}}
			<button type="submit" class="btn btn-primary">Editar</button>
			<a href="/razas" class="btn btn-primary">Ir a la lista</a>
		{{Form::close()}}

	</div>
@stop