@extends ('layouts.main')

@section('styles')
	<link rel="stylesheet" href="/styles/form.css">
@stop

@section('content')
	<div class="container content">
		<div class="form" role="form-inline">
			@if (isset($race))
				<h1>{{$race->title}}</h1>
				{{Form::model($race, ['method'=>'PUT', 'route'=>['razas.update', $race->id] ])}}
			@else
				<h1>Nueva raza</h1>
				{{Form::open(['url'=>'/razas', 'method'=>'POST'])}}
			@endif		
				<div class="form-group">
					{{Form::label('title', 'Raza', ['class' => 'form-label'])}}
					{{Form::text('title', null, ['class'=>'form-control'])}}	
				</div>
				<div class="form-group">
					{{Form::label('rr_cost', 'Coste RR', ['class' => 'form-label'])}}
					{{Form::text('rr_cost', null, ['class'=>'form-control'])}}
				</div>
				<div class="form-group">
					{{Form::label('allow_apo', 'Puede fichar apotecario', ['class' => 'form-label'])}}
					{{Form::select('allow_apo', [1=>'Sí', 0 => 'No'], null, ['class'=>'form-control'])}}
				</div>
				<button type="submit" class="btn btn-success">Guardar</button>
				<a href="/razas/{{ isset($race) ? $race->id : '' }}" class="btn btn-primary">Volver</a>
			{{Form::close()}}
		</div>
		@if (isset($race))
			<h3>Posiciones</h3>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Mo</th>
							<th>Fu</th>
							<th>Ag</th>
							<th>Ar</th>
							<th>Habilidades</th>
							<th>Coste</th>
							<th>Simp/Dob</th>
							<th><a class="btn btn-success btn-sm" href="/posiciones/create?races_id={{$race->id}}">+</a></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($race->positions as $position)
							<tr>
								<td><a class="text-warning" href="/posiciones/{{$position->id}}/edit">{{$position->title}}</a></td>
								<td>{{$position->mo}}</td>
								<td>{{$position->st}}</td>
								<td>{{$position->ag}}</td>
								<td>{{$position->ar}}</td>
								<td>
									@foreach ($position->skills as $skill)
										·<a href="/habilidades/{{$skill->id}}">{{$skill->title}}</a>
									@endforeach
								</td>
								<td>{{number_format($position->cost, 0, "", ".")}} <strong>mo.</strong></td>
								<td>
									@foreach ($position->simpleImprovements as $simple)
										·<a href="/tipos-habilidad/{{$simple->skill_type->id}}">{{$simple->skill_type->key}}</a>
									@endforeach
									/
									@foreach ($position->doubleImprovements as $double)
										·<a href="/tipos-habilidad/{{$double->skill_type->id}}">{{$double->skill_type->key}}·</a>
									@endforeach
								</td>
								<td>
									{{Form::model($position, ['method'=>'DELETE', 'route'=>['posiciones.destroy', $position->id], 'class'=>"deletePositionForm form-inline", "onsubmit"=>"return confirm('¿Está seguro de borrar la posición seleccionada?')" ])}}
										{{Form::submit('x', ['class' => 'btn btn-danger btn-sm'])}}
									{{Form::close()}}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="alert alert-danger" role="alert">
		    	<strong>¡Atención!</strong> Para crear las posiciones disponibles en el quipo primero debe guardarse con los datos básicos.
		    </div>
		@endif		
	</div>
@stop