@extends ('layouts.main')
@section('content')
	<div class="container">
		<div class="alert alert-danger" role="alert">
			Para acceder a esa página debe estar logueado
		</div>
		<div class="col-md-3" role="form">
			{{ Form::open(['url'=>'login', 'method'=>'POST', 'class'=>'form', 'role'=>'form']) }}
				<div class="form-group">
					{{Form::label('username', 'Usuario')}}
					{{Form::text('username', null, ['class'=>'form-control input-sm'])}}
				</div>
				<div class="form-group">
					{{Form::label('password', 'Contraseña')}}
					{{Form::password('username', ['class'=>'form-control input-sm'])}}
				</div>
				<div class="form-group">
					{{Form::submit('Sign in', ['class'=>'btn btn-success'])}}
				</div>
			{{Form::close()}}
		</div>
	</div>
@stop