@extends ('layouts.main')

@section('content')
	<div class="container">
		<h1>Habilidades <a class="btn btn-success" href="/habilidades/create">Añadir nueva</a></h1>
		
		<ul>
			@foreach($skills as $skill)
				<li><a href="/habilidades/{{$skill->id}}">{{$skill->title}} ({{$skill->type->title}})</a></li>
			@endforeach
		</ul>
	</div>
@stop