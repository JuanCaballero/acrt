@extends ('layouts.main')

@section('content')
	<div class="container">
		<h1>Tipos de habilidades</h1>
		<ul>
			@foreach($skillTypes as $skillType)
				<li><a href="/tipos-habilidad/{{$skillType->id}}">{{$skillType->title}}</a></li>
			@endforeach
		</ul>
	</div>
@stop