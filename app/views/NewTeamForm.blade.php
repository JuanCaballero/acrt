@extends ('layouts.main')

@section('styles')
	<link rel="stylesheet" href="/styles/teamForm.css">
@stop
@section('content')
	<div class="container">
		<h1 class="align-center">Nuevo Equipo</h1>
		<div class="form" role="form">
			<form action="/equipos" method="POST" class="form-inline">
				@foreach ($errors as $error)
					<div class="text-danger">{{$error}}</div>
				@endforeach
				{{Form::token()}}				
				<div class="form-group">
					<label for="title" class="sr-only">Nombre</label>
					<input type="text" name="title" class="form-control" placeholder="Nombre del equipo">
				</div>
				<div class="form-group">
					<label for="coach" class="sr-only">Entrenador</label>
					<input type="text" name="coach" class="form-control" placeholder="Entrenador">
				</div>
				<div class="form-group">
					<label for="divisions_id" class="sr-only">División</label>
					<select name="divisions_id" class="form-control" id="race">
						@foreach($divisions as $division)
							<option value="{{$division->id}}">{{$division->title}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="races_id" class="sr-only">Raza</label>
					<select name="races_id" class="form-control" id="race">
						@foreach($races as $race)
							<option value="{{$race->id}}">{{$race->title}}</option>
						@endforeach
					</select>
				</div>
					<button type="submit" class="btn btn-success">Crear</button>
			</form>
			<p class="text-danger">* ¡Ojo! Una vez creado el equipo la raza no podrá cambiarse</p>
		</div>
	</div>
@stop