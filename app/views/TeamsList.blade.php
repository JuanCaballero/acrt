@extends ('layouts.main')

@section('content')
	<div class="container">
		<h1>Equipos <a class="btn btn-success" href="/equipos/create">Añadir nuevo</a></h1>
		
		<ul>
			@foreach($teams as $team)
				<li><a href="/equipos/{{$team->id}}">{{$team->title}} ({{$team->race->title}})</a></li>
			@endforeach
		</ul>
	</div>
@stop