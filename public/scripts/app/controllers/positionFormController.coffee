class PositionFormController extends Monocle.Controller
	events:
		"click #addSkill" : "addSkill"
		"click #removeSkill" : "removeSkill"

	constructor:->
		super
		@skills = []

	addSkill:(event)->
		skillId =  $('select#skillsSelector').val()
		skillTitle = $('select#skillsSelector option:selected').text()
		view = new PositionSkillInputView 
		view.append {'skillId':skillId, 'skillTitle':skillTitle}

	removeSkill:(event)->
		skill_id =  $(event.currentTarget).attr('data-id')
		input = document.getElementsByName('skills[]')
		$(input).each (i, item)->
			if $(item).val() == skill_id
				$(item).remove()
				$('li#'+skill_id).remove()

class PositionSkillInputView extends Monocle.View
	container: '#skillsList'
	template:
        """
        	<input type='hidden' name='skills[]' value='{{skillId}}'>
        	<li id="{{skillId}}">
        		<h3>
        			<span class='label label-primary'>{{skillTitle}}</span>
        			<span id='removeSkill' data-id='{{skillId}}' class='label label-danger'>x</span>
        		</h3>
        	</li>
        """

__Controller.skillsFormController = new PositionFormController "#skillsContainer"