class TeamFormController extends Monocle.Controller
	events:
		"click #playersModal" : "showPlayerModal"
		"click #addPlayer" : "addPlayer"
		"click #removePlayer" : "removePlayer"
		"click #editModal" : "editModal"
		"click #updateField" : "updateField"
		"click #addSkill" : "addSkill"
		"click #removeSkill" : "removeSkill"
		"click #updatePlayerSkills" : "updatePlayerSkills"

	constructor: (tag, team, skillTypes)->
		super
		@team = team
		@positions = {}
		@skillTypes = skillTypes

		scope = @
		team.race.positions.forEach (item)->
			position = window.__Model.position.create(item)
			scope.positions[position.id] = position

		@fillPlayers(@)

	fillPlayers:(scope)->
		@team.players.forEach (player)->
			console.log player
			player.position = scope.positions[player.positions_id].title
			player.stringObject = JSON.stringify(player)
			row = new playerRow {container:"tr#"+player.number}
			row.html player

	showPlayerModal:(event)->
		$("#addPlayer").attr('data-number', $(event.currentTarget).parents('tr').attr('id'))
		$('#addPlayerModal').modal()

	addPlayer:(event)->
		$('#addPlayerModal').modal('hide')		
		number = $(event.currentTarget).attr 'data-number'
		name = $('input[name=playerName]').val()
		position = @positions[$('select[name=positions_id]').val()]
		player = 
			id:'new'
			number: number
			title: name
			position: position.title
			positions_id: position.id
			teams_id: @team.id
			races_id: @team.races_id
			mo: position.mo
			st: position.st
			ag: position.ag
			ar: position.ar
			skills: position.skills
			injuries: ''
			com: '0'
			td: '0'
			cas: '0'
			int: '0'
			mvp: '0'
			total_px: '0'
			cost: position.cost
			state: 'alive'

		player.stringObject = JSON.stringify(player)
		row = new playerRow {container:"tr#"+number}
		row.html player

	removePlayer:(event)->
		number = $(event.currentTarget).parents('tr').attr('id')
		emptyRow = new emptyplayerRow {container:"tr#"+number}
		emptyRow.html {number:number}

	editModal: (event)->
		if $(event.currentTarget).attr("data-type") != 'skills'
			@fieldModal(event)
		else
			@skillsModal(event)

	fieldModal:(event)->
		data = 
			label: $(event.currentTarget).attr('data-label')
			field: $(event.currentTarget).attr('data-type')
			value: $(event.currentTarget).html()
			number: $(event.currentTarget).parents('tr').attr('id')

		view = new playerFieldForm
		view.html data
		$("#editFieldModal").modal()

	updateField:(event)->
		number = $(event.currentTarget).attr('data-number')
		player = @getPlayerObject number
		player[$('#editFieldModal #fieldName').val()] = $('#editFieldModal #valueInput').val()

		player.stringObject = JSON.stringify(player)
		row = new playerRow {container:"tr#"+number}
		row.html player
		$('#editFieldModal').modal('hide')


	skillsModal:(event)->
		number = $(event.currentTarget).parents('tr').attr('id')
		str = $('tr#'+number).find('input[type=hidden]').val()
		player = @getPlayerObject number
		view = new playerSkillsForm
		view.html {skillTypes:@skillTypes, playerSkills:player.skills, number:number}
		$('#editSkillsModal').modal()

	addSkill:()->
		skillId = $('#skillsSelector').val()
		title = $('#skillsSelector option:selected').text()
		$('ul#skillsList').append '<li id="'+skillId+'"><h4><span id="title" class="btn btn-primary btn-sm">'+title+'</span> <span id="removeSkill" class="btn btn-sm btn-danger">x</span></h4></li>'

	removeSkill:(event)->
		$(event.currentTarget).parents('li').remove()

	updatePlayerSkills:(event)->
		number = $(event.currentTarget).attr('data-number')
		player = @getPlayerObject(number)
		console.log player
		skills = []
		$('ul#skillsList li').each (i, item)->
			skills.push( {id: $(item).attr('id'), title: $(item).find('span#title').html()} )

		player.skills = skills

		player.stringObject = JSON.stringify(player)
		row = new playerRow {container:"tr#"+number}
		row.html player
		$('#editSkillsModal').modal('hide')

	getPlayerObject:(number)->
		str = $('tr#'+number).find('input[type=hidden]').val()
		eval('player = '+str+';')
		return player

class playerRow extends Monocle.View
	template_url: "/scripts/app/templates/playerRow.mustache"

class emptyplayerRow extends Monocle.View
	template_url: "/scripts/app/templates/emptyPlayerRow.mustache"
	
class playerFieldForm extends Monocle.View
	container: "#editFieldModal"
	template_url: "/scripts/app/templates/playerFieldForm.mustache"

class playerSkillsForm extends Monocle.View
	container: "#editSkillsModal"
	template_url: "/scripts/app/templates/playerSkillsForm.mustache"