class __Model.position extends Monocle.Model
	@fields 'id', 'title', 'slug', 'mo', 'st', 'ag', 'ar', 'cost', 'races_id', 'max', 'skills'

	constructor:()->
		super

	create:()->
		skills = []
		@skills.forEach (item)->
			skills.push({id:item.id, title:item.title})
		
		@skills = skills
		super