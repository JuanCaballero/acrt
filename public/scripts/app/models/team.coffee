class __Model.team extends Monocle.Model
	@fields 'id', 'title', 'coach', 'races_id', 'ff', 'rrs', 'assistants', 'cheerleaders','has_apo','divisions_id','treasure','team_value','games','wins','losses','draws','bonus_td','malus_td','bonus_cas','malus_cas','points', 'comments'

	constructor:()->
		super
		